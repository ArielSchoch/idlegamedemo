﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TowerLevelDisplay : MonoBehaviour
{
    // TODO: Could make this class more reusable by using an IUpgradeable field instead.
    // However, Unity sadly doesn't support serialized interfaces.
    [SerializeField] private Tower tower;
    private Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
    }

    private void OnEnable()
    {
        tower.OnUpgrade += UpdateDisplay;
    }

    private void OnDisable()
    {
        tower.OnUpgrade -= UpdateDisplay;
    }

    public void UpdateDisplay(long value)
    {
        text.text = "Level " + (value + 1);
    }
}

