﻿using System.Collections;
using UnityEngine;
using Zenject;

public class ShowTowerStatsWindow : MonoBehaviour
{
    private TowerUpgradePanel towerUpgradePanel;

    [Inject]
    private void Init(TowerUpgradePanel towerUpgradePanel)
    {
        this.towerUpgradePanel = towerUpgradePanel;
    }

    public void ShowStats()
    {
        towerUpgradePanel.ShowStats();
    }
}
