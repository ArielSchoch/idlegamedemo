﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TowerUpgradePanel : MonoBehaviour
{
    [SerializeField] private Tower tower;
    [SerializeField] private Text headerText;
    [SerializeField] private Text totalTransportationText;
    [SerializeField] private Text transportersText;
    [SerializeField] private Text walkingSpeedText;
    [SerializeField] private Text loadPerTransporterText;
    [SerializeField] private Text loadingSpeedText;
    [SerializeField] private Text totalTransportationIncreaseText;
    [SerializeField] private Text transportersIncreaseText;
    [SerializeField] private Text walkingSpeedIncreaseText;
    [SerializeField] private Text loadPerTransporterIncreaseText;
    [SerializeField] private Text loadingSpeedIncreaseText;
    [SerializeField] private Text upgradeCostText;
    private int upgradeStep = 1;

    public void ShowStats()
    {
        this.gameObject.SetActive(true);
        UpdateStats();
    }

    public void SetUpgradeStep(float step)
    {
        upgradeStep = ((int) step == 1) ? 1 : ((int) step == 2) ? 10 : 50;
        UpdateStats();
    }

    public void Upgrade()
    {
        tower.Upgrade(upgradeStep);
        UpdateStats();
    }

    // Update the values in the upgrade panel UI
    private void UpdateStats()
    {
        var towerStats = tower.GetStats();
        headerText.text = "Tower Level " + (tower.Level + 1);

        totalTransportationText.text = towerStats.totalTransportation.FormatToCash() + "/s";
        transportersText.text = towerStats.transporters.ToString();
        walkingSpeedText.text = towerStats.walkingSpeed.ToString();
        loadPerTransporterText.text = towerStats.loadPerTransporter.FormatToCash();
        loadingSpeedText.text = towerStats.loadingSpeed.FormatToCash() + "/s";

        double currentTransportation = towerStats.totalTransportation;
        double upgradedTransportation = tower.GetStats(tower.Level + upgradeStep).totalTransportation;
        totalTransportationIncreaseText.text = "+" + (upgradedTransportation - currentTransportation).FormatToCash();

        int currentTransporters = towerStats.transporters;
        int upgradedTransporters = tower.GetStats(tower.Level + upgradeStep).transporters;
        transportersIncreaseText.text = "+" + (upgradedTransporters - currentTransporters);

        int currentWalkingSpeed = towerStats.walkingSpeed;
        int upgradedWalkingSpeed = tower.GetStats(tower.Level + upgradeStep).walkingSpeed;
        walkingSpeedIncreaseText.text = "+" + (upgradedWalkingSpeed - currentWalkingSpeed);

        double currentLoadingSpeed = towerStats.loadingSpeed;
        double upgradedLoadingSpeed = tower.GetStats(tower.Level + upgradeStep).loadingSpeed;
        loadingSpeedIncreaseText.text = "+" + (upgradedLoadingSpeed - currentLoadingSpeed).FormatToCash();

        double currentLoadPerTransporter = towerStats.loadPerTransporter;
        double upgradedLoadPerTransporter = tower.GetStats(tower.Level + upgradeStep).loadPerTransporter;
        loadPerTransporterIncreaseText.text = "+" + (upgradedLoadPerTransporter - currentLoadPerTransporter).FormatToCash();

        upgradeCostText.text = Enumerable.Range((int) tower.Level, upgradeStep).Sum(i => tower.GetStats(i).cost).FormatToCash();
    }
}
