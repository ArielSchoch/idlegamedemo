﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MineLevelDisplay : MonoBehaviour
{
    // TODO: Could make this class more reusable by using an IUpgradeable field instead.
    // However, Unity sadly doesn't support serialized interfaces.
    [SerializeField] private Mine mine;
    private Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
    }

    private void OnEnable()
    {
        mine.OnUpgrade += UpdateDisplay;
    }

    private void OnDisable()
    {
        mine.OnUpgrade -= UpdateDisplay;
    }

    public void UpdateDisplay(long value)
    {
        text.text = "Level " + (value + 1);
    }
}

