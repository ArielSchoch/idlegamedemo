﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MineCollectedDisplay : MonoBehaviour
{
    [SerializeField] private Mine mine;
    private Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
    }

    private void OnEnable()
    {
        mine.OnUpdateCollected += UpdateDisplay;
    }

    private void OnDisable()
    {
        mine.OnUpdateCollected -= UpdateDisplay;
    }

    public void UpdateDisplay(double amount)
    {
        text.text = amount.FormatToCash();
    }
}
