﻿using System.Collections;
using UnityEngine;
using Zenject;

public class ShowMineStatsWindow : MonoBehaviour
{
    [SerializeField] private Mine mine;
    private MineUpgradePanel mineUpgradePanel;

    [Inject]
    private void Init(MineUpgradePanel mineUpgradePanel)
    {
        this.mineUpgradePanel = mineUpgradePanel;
    }

    public void ShowStats()
    {
        mineUpgradePanel.ShowStats(mine);
        mineUpgradePanel.gameObject.SetActive(true);
    }
}
