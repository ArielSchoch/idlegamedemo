﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MineUpgradePanel : MonoBehaviour
{
    [SerializeField] private Text headerText;
    [SerializeField] private Text totalExtractionText;
    [SerializeField] private Text minersText;
    [SerializeField] private Text walkingSpeedText;
    [SerializeField] private Text miningSpeedText;
    [SerializeField] private Text totalExtractionIncreaseText;
    [SerializeField] private Text minersIncreaseText;
    [SerializeField] private Text walkingSpeedIncreaseText;
    [SerializeField] private Text miningSpeedIncreaseText;
    [SerializeField] private Text upgradeCostText;
    private int upgradeStep = 1;
    private Mine targetMine;

    public void ShowStats(Mine mine)
    {
        targetMine = mine;
        UpdateStats();
    }

    public void SetUpgradeStep(float step)
    {
        upgradeStep = ((int) step == 1) ? 1 : ((int) step == 2) ? 10 : 50;
        UpdateStats();
    }

    public void Upgrade()
    {
        targetMine.Upgrade(upgradeStep);
        UpdateStats();
    }

    // Update the values in the upgrade panel UI
    private void UpdateStats()
    {
        var mineStats = targetMine.GetLevelStats();
        headerText.text = string.Format("Mine Shaft {0} Level {1}", 1, targetMine.Level + 1);

        totalExtractionText.text = mineStats.totalExtraction.FormatToCash() + "/s";
        minersText.text = mineStats.miners.ToString();
        walkingSpeedText.text = mineStats.walkingSpeed.ToString();
        miningSpeedText.text = mineStats.miningSpeed.FormatToCash() + "/s";

        double currentExtraction = mineStats.totalExtraction;
        double upgradedExtraction = targetMine.GetLevelStats(targetMine.Level + upgradeStep).totalExtraction;
        totalExtractionIncreaseText.text = "+" + (upgradedExtraction - currentExtraction).FormatToCash();

        int currentMiners = mineStats.miners;
        int upgradedMiners = targetMine.GetLevelStats(targetMine.Level + upgradeStep).miners;
        minersIncreaseText.text = "+" + (upgradedMiners - currentMiners);

        int currentWalkingSpeed = mineStats.walkingSpeed;
        int upgradedWalkingSpeed = targetMine.GetLevelStats(targetMine.Level + upgradeStep).walkingSpeed;
        walkingSpeedIncreaseText.text = "+" + (upgradedWalkingSpeed - currentWalkingSpeed);

        double currentMiningSpeed = mineStats.miningSpeed;
        double upgradedMiningSpeed = targetMine.GetLevelStats(targetMine.Level + upgradeStep).miningSpeed;
        miningSpeedIncreaseText.text = "+" + (upgradedMiningSpeed - currentMiningSpeed).FormatToCash();

        upgradeCostText.text = Enumerable.Range((int) targetMine.Level, upgradeStep).Sum(i => targetMine.GetLevelStats(i).cost).FormatToCash();
    }
}
