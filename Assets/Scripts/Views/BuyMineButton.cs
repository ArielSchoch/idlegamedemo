﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

/* Contains mine purchase button logic. */

public class BuyMineButton : MonoBehaviour
{
    [SerializeField] private Text priceText;
    [SerializeField] private MineFactory mineFactory;
    [SerializeField] private MineUpgradeData mineStats;
    private GameManager gameManager;
    private CashManager cashManager;

    [Inject]
    private void Init(GameManager gameManager, CashManager cashManager)
    {
        this.gameManager = gameManager;
        this.cashManager = cashManager;
    }

    private void Start()
    {
        UpdateDisplay();
    }

    public void BuyMine()
    {
        double price = GetNextMinePrice(gameManager.GetMineCount());
        if (cashManager.CanBuy(price))
        {
            cashManager.SpendCash(price);
            gameManager.CreateMine();
            UpdateDisplay();
        }
    }

    // Update the mine purchase price text and button position
    public void UpdateDisplay()
    {
        Vector3 pos = this.transform.position;
        pos.y = mineFactory.GetMineWorldPosition(gameManager.GetMineCount());
        this.transform.position = pos;
        priceText.text = GetNextMinePrice(gameManager.GetMineCount()).FormatToCash();
    }

    // Get the cost of the next mine shaft
    private double GetNextMinePrice(int shaftLevel)
    {
        var mineType = mineStats.mineTypes[(int) gameManager.GetMineType()];
        var shaftLevelInfo = mineType.shaftLevels[Mathf.Clamp(shaftLevel, 0, mineType.shaftLevels.Length - 1)];
        return shaftLevelInfo.mineUpgrades[0].cost;
    }
}
