﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ElevatorUpgradePanel : MonoBehaviour
{
    [SerializeField] private Elevator elevator;
    [SerializeField] private Text headerText;
    [SerializeField] private Text totalTransportationText;
    [SerializeField] private Text loadText;
    [SerializeField] private Text movementSpeedText;
    [SerializeField] private Text loadingSpeedText;
    [SerializeField] private Text totalTransportationIncreaseText;
    [SerializeField] private Text loadIncreaseText;
    [SerializeField] private Text movementSpeedIncreaseText;
    [SerializeField] private Text loadingSpeedIncreaseText;
    [SerializeField] private Text upgradeCostText;
    private int upgradeStep = 1;

    public void ShowStats()
    {
        this.gameObject.SetActive(true);
        UpdateStats();
    }

    public void SetUpgradeStep(float step)
    {
        upgradeStep = ((int) step == 1) ? 1 : ((int) step == 2) ? 10 : 50;
        UpdateStats();
    }

    public void Upgrade()
    {
        elevator.Upgrade(upgradeStep);
        UpdateStats();
    }

    // Update the values in the upgrade panel UI
    private void UpdateStats()
    {
        var elevatorStats = elevator.GetStats();
        headerText.text = "Elevator Level " + (elevator.Level + 1);

        totalTransportationText.text = elevatorStats.totalTransportation.FormatToCash() + "/s";
        loadText.text = elevatorStats.load.FormatToCash();
        movementSpeedText.text = elevatorStats.movementSpeed.ToString("0.00");
        loadingSpeedText.text = elevatorStats.loadingSpeed.FormatToCash() + "/s";

        double currentTransportation = elevatorStats.totalTransportation;
        double upgradedTransportation = elevator.GetStats(elevator.Level + upgradeStep).totalTransportation;
        totalTransportationIncreaseText.text = "+" + (upgradedTransportation - currentTransportation).FormatToCash();

        double currentLoad = elevatorStats.load;
        double upgradedLoad = elevator.GetStats(elevator.Level + upgradeStep).load;
        loadIncreaseText.text = "+" + (upgradedLoad - currentLoad).FormatToCash();

        double currentMovementSpeed = elevatorStats.movementSpeed;
        double upgradedMovementSpeed = elevator.GetStats(elevator.Level + upgradeStep).movementSpeed;
        movementSpeedIncreaseText.text = "+" + (upgradedMovementSpeed - currentMovementSpeed).ToString("0.00");

        double currentLoadingSpeed = elevatorStats.loadingSpeed;
        double upgradedLoadingSpeed = elevator.GetStats(elevator.Level + upgradeStep).loadingSpeed;
        loadingSpeedIncreaseText.text = "+" + (upgradedLoadingSpeed - currentLoadingSpeed).FormatToCash();

        upgradeCostText.text = Enumerable.Range((int) elevator.Level, upgradeStep).Sum(i => elevator.GetStats(i).cost).FormatToCash();
    }
}
