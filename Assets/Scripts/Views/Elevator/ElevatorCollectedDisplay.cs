﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ElevatorCollectedDisplay : MonoBehaviour
{
    [SerializeField] private Elevator elevator;
    private Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
    }

    private void OnEnable()
    {
        elevator.OnUpdateCollected += UpdateDisplay;
    }

    private void OnDisable()
    {
        elevator.OnUpdateCollected -= UpdateDisplay;
    }

    public void UpdateDisplay(double amount)
    {
        text.text = amount.FormatToCash();
    }
}
