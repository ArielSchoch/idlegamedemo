﻿using System.Collections;
using UnityEngine;
using Zenject;

public class ShowElevatorStatsWindow : MonoBehaviour
{
    private ElevatorUpgradePanel elevatorUpgradePanel;

    [Inject]
    private void Init(ElevatorUpgradePanel elevatorUpgradePanel)
    {
        this.elevatorUpgradePanel = elevatorUpgradePanel;
    }

    public void ShowStats()
    {
        elevatorUpgradePanel.ShowStats();
    }
}
