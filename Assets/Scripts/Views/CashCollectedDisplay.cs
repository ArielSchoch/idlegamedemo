﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

/* Simply updates the cash UI when value changes. */

[RequireComponent(typeof(Text))]
public class CashCollectedDisplay : MonoBehaviour
{
    private CashManager cashManager;
    private Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
    }

    [Inject]
    private void Init(CashManager cashManager)
    {
        this.cashManager = cashManager;
    }

    private void OnEnable()
    {
        cashManager.OnUpdateBalance += UpdateDisplay;
    }

    private void OnDisable()
    {
        cashManager.OnUpdateBalance -= UpdateDisplay;
    }

    public void UpdateDisplay(double amount)
    {
        text.text = amount.FormatToCash();
    }
}
