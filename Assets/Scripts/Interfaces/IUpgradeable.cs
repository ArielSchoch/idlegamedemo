using System;

public interface IUpgradeable
{
    event Action<long> OnUpgrade;
    void Upgrade(long levels);
}