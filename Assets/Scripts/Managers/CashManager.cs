﻿using System;
using UnityEngine;

public class CashManager : MonoBehaviour
{
    public event Action<double> OnUpdateBalance;
    private double cash;
    public double Cash { get { return cash; } set { cash = value; } }
    private double startCash = 20;
    public double StartCash { get { return startCash; } }

    private void Start()
    {
        UpdateBalance();
    }

    public void EarnCash(double amount)
    {
        cash += amount;
        UpdateBalance();
    }

    public void SpendCash(double amount)
    {
        cash = System.Math.Max(cash - amount, 0d);
        UpdateBalance();
    }

    public bool CanBuy(double price)
    {
        return cash >= price;
    }

    private void UpdateBalance()
    {
        var eventHandler = OnUpdateBalance;
        if (eventHandler != null)
            eventHandler(cash);
    }
}
