﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private LevelData levelData;
    [SerializeField] private CashManager cashManager;
    [SerializeField] private MineFactory mineFactory;
    [SerializeField] private Elevator elevator;
    [SerializeField] private Tower tower;
    [SerializeField] private GameProgress gameProgress;
    private List<Mine> mines = new List<Mine>();
    private int currentLevelIndex;
    private const string GameProgressFilePath = "/GameProgress.gd";

    private void Awake()
    {
        LoadLevelStats(0);
    }

    public int GetMineCount()
    {
        return mines.Count;
    }

    public Mine GetMine(int index)
    {
        return mines[index];
    }

    public Mine.Type GetMineType()
    {
        return levelData.levels[currentLevelIndex].mineType;
    }

    public void CreateMine()
    {
        CreateMine(mines.Count);
    }

    public void CreateMine(int shaftIndex)
    {
        mines.Add(mineFactory.Create(GetMineType(), shaftIndex));
    }

    public void LoadLevelStats(int levelIndex)
    {
        // Load serialized GameProgress class data from disc
        if (File.Exists(Application.persistentDataPath + GameProgressFilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + GameProgressFilePath, FileMode.Open);
            gameProgress = (GameProgress) bf.Deserialize(file);
            file.Close();
        }
        else gameProgress.cashTotal = cashManager.StartCash;

        cashManager.Cash = gameProgress.cashTotal;
        currentLevelIndex = levelIndex;
        if (levelIndex < gameProgress.unlockedLevels.Count)
        {
            LevelProgress levelProgress = gameProgress.unlockedLevels[levelIndex];
            tower.Initialize(levelProgress.tower.level);
            elevator.Initialize(levelProgress.elevator.level, levelProgress.elevator.collected);

            for (int i = 0; i < levelProgress.unlockedMineShafts.Count; i++)
                mines.Add(mineFactory.Create(GetMineType(), i, levelProgress.unlockedMineShafts[i]));
        }
    }

    private void OnApplicationQuit()
    {
        SaveGameProgress();
    }

    public void SaveGameProgress()
    {
        gameProgress.cashTotal = cashManager.Cash;

        LevelProgress levelProgress;
        if (gameProgress.unlockedLevels.Count == 0)
        {
            levelProgress = new LevelProgress();
            levelProgress.tower = new TowerStats();
            levelProgress.elevator = new ElevatorStats();
            levelProgress.unlockedMineShafts = new List<MineStats>();
            gameProgress.unlockedLevels.Add(levelProgress);
        }
        else levelProgress = gameProgress.unlockedLevels[currentLevelIndex];

        levelProgress.tower.level = tower.Level;
        levelProgress.elevator.level = elevator.Level;
        levelProgress.elevator.collected = elevator.Collected;

        levelProgress.unlockedMineShafts.Clear();
        for (int i = 0; i < mines.Count; i++)
        {
            var mineStats = new MineStats();
            mineStats.level = mines[i].Level;
            mineStats.collected = mines[i].Collected;
            levelProgress.unlockedMineShafts.Add(mineStats);
        }

        // Save GameProgress class to disc
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + GameProgressFilePath);
        bf.Serialize(file, gameProgress);
        file.Close();
    }

    public void ResetProgress()
    {
        if (File.Exists(Application.persistentDataPath + GameProgressFilePath))
            File.Delete(Application.persistentDataPath + GameProgressFilePath);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
