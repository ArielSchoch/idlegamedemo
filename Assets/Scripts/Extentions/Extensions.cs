﻿using System;
using System.Globalization;
using System.Linq;
using UnityEngine;

public static class Extensions
{
    private static string[] suffixes;

    static Extensions()
    {
        suffixes = new string[5 + 26 * 26];
        suffixes[0] = "";
        suffixes[1] = "K";
        suffixes[2] = "M";
        suffixes[3] = "B";
        suffixes[4] = "T";

        // Add values aa, ab, ac, ..., ba, bb, bc, ...
        char[] alphabet = Enumerable.Range('a', 26).Select(n => (char) n).ToArray();
        for (int i = 0; i < alphabet.Length; i++)
            for (int j = 0; j < alphabet.Length; j++)
                suffixes[5 + i * 26 + j] = string.Concat(alphabet[i], alphabet[j]);
    }

    public static T GetValueOrDefault<T>(this T[] arr, int index, T defaultValue)
    {
        return index < 0 || index >= arr.Length ? defaultValue : arr[index];
    }

    public static string FormatToCash(this double amount)
    {
        if (amount == 0d) return "0";

        int parts = (int) (Math.Log10(amount) / 3d);
        double result = amount / Math.Pow(1000d, parts);
        return result.ToString("0.##" + suffixes.GetValueOrDefault(parts, ""), CultureInfo.InvariantCulture);
    }
}
