﻿using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/*
Convenience class that automatically generates ElevatorUpgradeData values based on a simple quadratic curve.
Could instead import data from Excel or similar file types.
*/

namespace EditorUtilities
{
    [Serializable]
    public class ElevatorUpgradeSettings
    {
        public int numberOfUpgradeLevels = 100;
        public double startLoad;
        public double loadUpgradeFactor;
        public double startMovementSpeed;
        public double movementSpeedUpgradeFactor;
        public double startLoadingSpeed;
        public double loadingSpeedUpgradeFactor;
        public double startCost;
        public double costUpgradeFactor;
    }

    public class ElevatorUpgradeDataCreator : MonoBehaviour
    {
        [SerializeField] private ElevatorUpgradeData upgradeData;
        [SerializeField] private ElevatorUpgradeSettings upgradeSettings;

        private void Start()
        {
            GenerateUpgradeData();
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void GenerateUpgradeData()
        {
            Debug.Log("Created ElevatorUpgradeData values.");
            var elevatorUpgrades = new ElevatorUpgrade[upgradeSettings.numberOfUpgradeLevels];
            for (int i = 0; i < upgradeSettings.numberOfUpgradeLevels; i++)
                elevatorUpgrades[i] = CreateUpgradeLevelData(i);

            upgradeData.upgrades = elevatorUpgrades;
            #if UNITY_EDITOR
            EditorUtility.SetDirty(upgradeData);
            #endif
        }

        private ElevatorUpgrade CreateUpgradeLevelData(long level)
        {
            var upgradeLevel = new ElevatorUpgrade();
            upgradeLevel.load = GetQuatraticCurveValue(level, upgradeSettings.startLoad, upgradeSettings.loadUpgradeFactor);
            upgradeLevel.movementSpeed = (float) GetQuatraticCurveValue(level, upgradeSettings.startMovementSpeed, upgradeSettings.movementSpeedUpgradeFactor);
            upgradeLevel.cost = GetQuatraticCurveValue(level, upgradeSettings.startCost, upgradeSettings.costUpgradeFactor);
            upgradeLevel.loadingSpeed = GetQuatraticCurveValue(level, upgradeSettings.startLoadingSpeed, upgradeSettings.loadingSpeedUpgradeFactor);
            upgradeLevel.totalTransportation = upgradeLevel.loadingSpeed / (6d / upgradeLevel.movementSpeed);
            return upgradeLevel;
        }

        // Upgrade curve defined as: y = n + (x^2 + x) * n * f
        private double GetQuatraticCurveValue(long level, double startValue, double factor)
        {
            return startValue + (level * level + level) * startValue * factor;
        }
    }
}
