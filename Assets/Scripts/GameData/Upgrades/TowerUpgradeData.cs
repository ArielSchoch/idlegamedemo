﻿using System;
using UnityEngine;

/* Contains all the level data required to allow the tower to be upgraded. */

public class TowerUpgradeData : ScriptableObject
{
    public TowerUpgrade[] upgrades;
}

[Serializable]
public class TowerUpgrade
{
    public double totalTransportation;
    public int transporters;
    public int walkingSpeed;
    public double loadPerTransporter;
    public double loadingSpeed;
    public double cost;
}
