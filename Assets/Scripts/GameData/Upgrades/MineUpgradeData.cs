﻿using System;
using UnityEngine;

/* Contains all the level data required to allow mines to be upgraded. */

public class MineUpgradeData : ScriptableObject
{
    [Header("New entry for each resource type (coal, gold, ...)")]
    public MineType[] mineTypes;
}

[Serializable]
public class MineType
{
    public string resourceType;
    public ShaftLevel[] shaftLevels;
}

[Serializable]
public class ShaftLevel
{
    public MineUpgrade[] mineUpgrades;
}

[Serializable]
public class MineUpgrade
{
    public double totalExtraction;
    public int miners;
    public int walkingSpeed;
    public double miningSpeed;
    public double cost;
}
