﻿using System;
using UnityEngine;

/* Contains all the level data required to allow the elevator to be upgraded. */

public class ElevatorUpgradeData : ScriptableObject
{
    public ElevatorUpgrade[] upgrades;
}

[Serializable]
public class ElevatorUpgrade
{
    public double totalTransportation;
    public double load;
    public float movementSpeed;
    public double loadingSpeed;
    public double cost;
}
