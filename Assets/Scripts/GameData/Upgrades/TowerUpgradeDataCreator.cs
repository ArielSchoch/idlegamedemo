﻿using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/*
Convenience class that automatically generates TowerUpgradeData values based on a simple quadratic curve.
Could instead import data from Excel or similar file types.
*/

namespace EditorUtilities
{
    [Serializable]
    public class TowerUpgradeSettings
    {
        public int numberOfUpgradeLevels = 100;
        public double startLoadPerTransporter;
        public double loadPerTransporterUpgradeFactor;
        public double startLoadingSpeed;
        public double loadingSpeedUpgradeFactor;
        public double startCost;
        public double costUpgradeFactor;
    }

    public class TowerUpgradeDataCreator : MonoBehaviour
    {
        [SerializeField] private TowerUpgradeData upgradeData;
        [SerializeField] private int[] transportersIncreaseAtLevels;
        [SerializeField] private int[] walkingSpeedIncreaseAtLevels;
        [SerializeField] private TowerUpgradeSettings upgradeSettings;

        private void Start()
        {
            GenerateUpgradeData();
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void GenerateUpgradeData()
        {
            Debug.Log("Created TowerUpgradeData values.");
            var towerUpgrades = new TowerUpgrade[upgradeSettings.numberOfUpgradeLevels];
            for (int i = 0; i < upgradeSettings.numberOfUpgradeLevels; i++)
                towerUpgrades[i] = CreateUpgradeLevelData(i);

            upgradeData.upgrades = towerUpgrades;
            #if UNITY_EDITOR
            EditorUtility.SetDirty(upgradeData);
            #endif
        }

        private TowerUpgrade CreateUpgradeLevelData(long level)
        {
            var upgradeLevel = new TowerUpgrade();
            upgradeLevel.transporters = 1 + transportersIncreaseAtLevels.Sum(i => (i <= level + 1) ? 1 : 0);
            upgradeLevel.walkingSpeed = 1 + walkingSpeedIncreaseAtLevels.Sum(i => (i <= level + 1) ? 1 : 0);
            upgradeLevel.loadPerTransporter = GetQuatraticCurveValue(level, upgradeSettings.startLoadPerTransporter, upgradeSettings.loadPerTransporterUpgradeFactor);
            upgradeLevel.loadingSpeed = GetQuatraticCurveValue(level, upgradeSettings.startLoadingSpeed, upgradeSettings.loadingSpeedUpgradeFactor);
            upgradeLevel.cost = GetQuatraticCurveValue(level, upgradeSettings.startCost, upgradeSettings.costUpgradeFactor);
            upgradeLevel.totalTransportation = upgradeLevel.loadingSpeed / (6d / upgradeLevel.walkingSpeed);
            return upgradeLevel;
        }

        // Upgrade curve defined as: y = n + (x^2 + x) * n * f
        private double GetQuatraticCurveValue(long level, double startValue, double factor)
        {
            return startValue + (level * level + level) * startValue * factor;
        }
    }
}
