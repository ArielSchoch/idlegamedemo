﻿using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/*
Convenience class that automatically generates MineUpgradeData values based on a simple quadratic curve.
Could instead import data from Excel or similar file types.
*/

namespace EditorUtilities
{
    [Serializable]
    public class MineTypeSettings
    {
        public string resourceType;
        public ShaftLevelSettings[] shaftLevelSettings;
    }

    [Serializable]
    public class ShaftLevelSettings
    {
        public int numberOfUpgradeLevels = 100;
        public int[] minerIncreaseAtLevels;
        public int[] speedIncreaseAtLevels;
        public UpgradeLevelSettings upgradeLevelSettings;
    }

    [Serializable]
    public class UpgradeLevelSettings
    {
        public double startMiningSpeed;
        public double miningUpgradeFactor;
        public double startCost;
        public double costUpgradeFactor;
    }

    public class MineUpgradeDataCreator : MonoBehaviour
    {
        [SerializeField] private MineUpgradeData mineUpgradeData;
        [SerializeField] private MineTypeSettings[] mineTypeSettings;
        private const double startWalkSpeedDivisor = 1.74d;
        private const double walkSpeedFactor = 0.12d;

        private void Start()
        {
            GenerateUpgradeData();
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void GenerateUpgradeData()
        {
            Debug.Log("Created MineUpgradeData values.");
            var mineTypes = new MineType[mineTypeSettings.Length];
            // For each resource type defined (coal, gold, ...)
            for (int i = 0; i < mineTypes.Length; i++)
            {
                var shaftLevels = new ShaftLevel[mineTypeSettings[i].shaftLevelSettings.Length];
                // For each shaft level defined
                for (int j = 0; j < shaftLevels.Length; j++)
                {
                    var shaftLevelSetting = mineTypeSettings[i].shaftLevelSettings[j];
                    var mineUpgrades = new MineUpgrade[shaftLevelSetting.numberOfUpgradeLevels];
                    // For each upgrade level defined, generate values based on settings
                    for (int k = 0; k < mineUpgrades.Length; k++)
                        mineUpgrades[k] = CreateUpgradeLevelData(k, shaftLevelSetting);

                    shaftLevels[j] = new ShaftLevel();
                    shaftLevels[j].mineUpgrades = mineUpgrades;
                }
                mineTypes[i] = new MineType();
                mineTypes[i].resourceType = mineTypeSettings[i].resourceType;
                mineTypes[i].shaftLevels = shaftLevels;
            }
            mineUpgradeData.mineTypes = mineTypes;
            #if UNITY_EDITOR
            EditorUtility.SetDirty(mineUpgradeData);
            #endif
        }

        private MineUpgrade CreateUpgradeLevelData(long level, ShaftLevelSettings settings)
        {
            var upgradeLevel = new MineUpgrade();
            upgradeLevel.miners = 1 + settings.minerIncreaseAtLevels.Sum(i => (i <= level + 1) ? 1 : 0);
            upgradeLevel.walkingSpeed = 1 + settings.speedIncreaseAtLevels.Sum(i => (i <= level + 1) ? 1 : 0);
            upgradeLevel.cost = GetQuatraticCurveValue(level, settings.upgradeLevelSettings.startCost, settings.upgradeLevelSettings.costUpgradeFactor);
            upgradeLevel.miningSpeed = GetQuatraticCurveValue(level, settings.upgradeLevelSettings.startMiningSpeed, settings.upgradeLevelSettings.miningUpgradeFactor);
            upgradeLevel.totalExtraction = CalculateTotalExtractionAtLevel(level, upgradeLevel.miners, upgradeLevel.walkingSpeed, upgradeLevel.miningSpeed);
            return upgradeLevel;
        }

        private double CalculateTotalExtractionAtLevel(long level, int miners, int walkingSpeed, double miningSpeed)
        {
            return (miningSpeed * miners) / (startWalkSpeedDivisor - (walkingSpeed - 1) * walkSpeedFactor);
        }

        // Upgrade curve defined as: y = n + (x^2 + x) * n * f
        private double GetQuatraticCurveValue(long level, double startValue, double factor)
        {
            return startValue + (level * level + level) * startValue * factor;
        }
    }
}
