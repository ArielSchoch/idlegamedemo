﻿using UnityEngine;

/* Contains settings for each game level the player can visit. */

public class LevelData : ScriptableObject
{
    public Level[] levels;
}

[System.Serializable]
public struct Level
{
    public Mine.Type mineType;
}
