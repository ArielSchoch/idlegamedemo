﻿using System;
using System.Collections.Generic;
using UnityEngine;

/* Contains all the level progress info required to save the game state. */

[Serializable]
public class GameProgress //: ScriptableObject
{
    public double cashTotal;
    public List<LevelProgress> unlockedLevels;
}

[Serializable]
public class LevelProgress
{
    public TowerStats tower;
    public ElevatorStats elevator;
    public List<MineStats> unlockedMineShafts;
}

[Serializable]
public class TowerStats
{
    public long level;
}

[Serializable]
public class ElevatorStats
{
    public long level;
    public double collected;
}

[Serializable]
public class MineStats
{
    public long level;
    public double collected;
}
