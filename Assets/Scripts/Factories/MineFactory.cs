﻿using System;
using UnityEngine;
using Zenject;

public class MinePrefabFactory : PrefabFactory<Mine> {}

public class MineFactory : MonoBehaviour
{
    [SerializeField] private float startPosY;
    [SerializeField] private float mineHeight;
    [SerializeField] private GameObject minePrefab;
    [SerializeField] private Transform parent;
    private MinePrefabFactory minePrefabFactory;

    [Inject]
    private void Init(MinePrefabFactory minePrefabFactory)
    {
        this.minePrefabFactory = minePrefabFactory;
    }

    public float GetMineWorldPosition(int index)
    {
        return startPosY - index * mineHeight;
    }

    // Create a new mine prefab instance
    public Mine Create(Mine.Type resourceType, int shaftIndex, MineStats stats = null)
    {
        Mine mineInstance = minePrefabFactory.Create(minePrefab);
        Vector3 pos = mineInstance.transform.position;
        pos.y = GetMineWorldPosition(shaftIndex);
        mineInstance.transform.position = pos;

        if (stats == null)
            mineInstance.Initialize(resourceType, shaftIndex, 0, 0);
        else
            mineInstance.Initialize(resourceType, shaftIndex, stats.level, stats.collected);

        mineInstance.transform.SetParent(parent);
        return mineInstance;
    }
}
