﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Zenject;

public class Tower : MonoBehaviour
{
    public event Action<long> OnUpgrade;
    [SerializeField] private TowerUpgradeData towerStats;
    [SerializeField] private Elevator elevator;
    public long Level { get { return level; } }
    private long level;
    private CashManager cashManager;

    [Inject]
    private void Init(CashManager cashManager)
    {
        this.cashManager = cashManager;
    }

    public void Initialize(long upgradeLevel)
    {
        this.level = upgradeLevel;
        Upgrade(0);
    }

    private void Start()
    {
        StartCoroutine(Collect());
    }

    private IEnumerator Collect()
    {
        while (true)
        {
            var stats = GetStats();
            double tripDuration = 10d / stats.walkingSpeed;
            yield return new WaitForSeconds((float) tripDuration);
            double collectionAmount = Math.Min(elevator.Collected, GetStats().totalTransportation * tripDuration);
            elevator.RemoveCollected(collectionAmount);
            cashManager.EarnCash(collectionAmount);
        }
    }

    public TowerUpgrade GetStats()
    {
        return GetStats(level);
    }

    public TowerUpgrade GetStats(long level)
    {
        return towerStats.upgrades[Mathf.Clamp((int) level, 0, towerStats.upgrades.Length - 1)];
    }

    public void Upgrade(long levels)
    {
        double price = Enumerable.Range((int) level, (int) levels).Sum(i => GetStats(i).cost);
        if (cashManager.CanBuy(price))
        {
            cashManager.SpendCash(price);
            level += levels;
            var eventHandler = OnUpgrade;
            if (eventHandler != null)
                eventHandler(level);
        }
    }
}
