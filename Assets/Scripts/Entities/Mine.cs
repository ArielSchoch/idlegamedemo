﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Zenject;

public class Mine : MonoBehaviour, IUpgradeable
{
    public event Action<long> OnUpgrade;
    public event Action<double> OnUpdateCollected;
    [SerializeField] protected MineUpgradeData mineStats;
    public enum Type { Coal, Gold, Ruby, Diamond, Emerald }
    protected Type type;
    public long ShaftLevel { get { return shaftLevel; } }
    protected long shaftLevel;
    public long Level { get { return level; } }
    protected long level;
    public double Collected { get { return collected; } }
    protected double collected = 0;
    protected double miningDuration = 5d;
    private CashManager cashManager;

    [Inject]
    private void Init(CashManager cashManager)
    {
        this.cashManager = cashManager;
    }

    public void Initialize(Type type, long shaftLevel, long upgradeLevel, double collected)
    {
        this.type = type;
        this.shaftLevel = shaftLevel;
        this.level = upgradeLevel;
        this.collected = collected;
        UpdateCollected(collected);
        Upgrade(0);
    }

    private void Start()
    {
        StartCoroutine(Collect());
    }

    private IEnumerator Collect()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            collected += Math.Round(GetLevelStats().totalExtraction * miningDuration, 0);
            UpdateCollected(collected);
        }
    }

    public void RemoveCollected(double amount)
    {
        collected = Math.Round(collected - amount, 0);
        UpdateCollected(collected);
    }

    public MineUpgrade GetLevelStats()
    {
        return GetLevelStats(level);
    }

    public MineUpgrade GetLevelStats(long level)
    {
        var mineType = mineStats.mineTypes[(int) type];
        var shaftLevelInfo = mineType.shaftLevels[Mathf.Clamp((int) shaftLevel, 0, mineType.shaftLevels.Length - 1)];
        return shaftLevelInfo.mineUpgrades[Mathf.Clamp((int) level, 0, shaftLevelInfo.mineUpgrades.Length - 1)];
    }

    public virtual void Upgrade(long levels)
    {
        double price = Enumerable.Range((int) level, (int) levels).Sum(i => GetLevelStats(i).cost);
        if (cashManager.CanBuy(price))
        {
            cashManager.SpendCash(price);
            level += levels;
            var eventHandler = OnUpgrade;
            if (eventHandler != null)
                eventHandler(level);
        }
    }

    protected void UpdateCollected(double amount)
    {
        var eventHandler = OnUpdateCollected;
        if (eventHandler != null)
            eventHandler(amount);
    }
}
