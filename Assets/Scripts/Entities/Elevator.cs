﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Zenject;

public class Elevator : MonoBehaviour
{
    public event Action<long> OnUpgrade;
    public event Action<double> OnUpdateCollected;
    [SerializeField] private ElevatorUpgradeData elevatorStats;
    public long Level { get { return level; } }
    private long level;
    public double Collected { get { return collected; } }
    private double collected = 0;
    private GameManager gameManager;
    private CashManager cashManager;

    [Inject]
    private void Init(GameManager gameManager, CashManager cashManager)
    {
        this.gameManager = gameManager;
        this.cashManager = cashManager;
    }

    public void Initialize(long upgradeLevel, double collected)
    {
        this.level = upgradeLevel;
        this.collected = collected;
        UpdateCollected(collected);
        Upgrade(0);
    }

    private void Start()
    {
        StartCoroutine(Collect());
    }

    private IEnumerator Collect()
    {
        // Add a random offset to prevent tower, elevator and mines from collecting at the same time
        yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 2f));
        while (true)
        {
            var stats = GetStats();
            double tripDuration = 2d / stats.movementSpeed;
            yield return new WaitForSeconds((float) tripDuration);

            double maxCollection = Math.Round(Math.Min(stats.load - collected, stats.totalTransportation * tripDuration), 0);
            double totalCollected = 0;
            for (int i = 0; i < gameManager.GetMineCount(); i++)
            {
                double collection = Math.Min(gameManager.GetMine(i).Collected, Math.Max(0, maxCollection - totalCollected));
                totalCollected += collection;
                gameManager.GetMine(i).RemoveCollected(collection);
            }
            collected += totalCollected;
            UpdateCollected(collected);
        }
    }

    public void RemoveCollected(double amount)
    {
        collected = Math.Round(collected - amount, 0);
        UpdateCollected(collected);
    }

    public ElevatorUpgrade GetStats()
    {
        return GetStats(level);
    }

    public ElevatorUpgrade GetStats(long level)
    {
        return elevatorStats.upgrades[Mathf.Clamp((int) level, 0, elevatorStats.upgrades.Length - 1)];
    }

    public virtual void Upgrade(long levels)
    {
        double price = Enumerable.Range((int) level, (int) levels).Sum(i => GetStats(i).cost);
        if (cashManager.CanBuy(price))
        {
            cashManager.SpendCash(price);
            level += levels;
            var eventHandler = OnUpgrade;
            if (eventHandler != null)
                eventHandler(level);
        }
    }

    private void UpdateCollected(double amount)
    {
        var eventHandler = OnUpdateCollected;
        if (eventHandler != null)
            eventHandler(amount);
    }
}
