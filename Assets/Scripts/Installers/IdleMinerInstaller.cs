﻿using UnityEngine;
using Zenject;

public class IdleMinerInstaller : MonoInstaller
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private CashManager cashManager;
    [SerializeField] private TowerUpgradePanel towerUpgradePanel;
    [SerializeField] private ElevatorUpgradePanel elevatorUpgradePanel;
    [SerializeField] private MineUpgradePanel mineUpgradePanel;

    public override void InstallBindings()
    {
        Container.Bind<GameManager>().FromInstance(gameManager);
        Container.Bind<CashManager>().FromInstance(cashManager);
        Container.Bind<TowerUpgradePanel>().FromInstance(towerUpgradePanel);
        Container.Bind<ElevatorUpgradePanel>().FromInstance(elevatorUpgradePanel);
        Container.Bind<MineUpgradePanel>().FromInstance(mineUpgradePanel);
        Container.Bind<MinePrefabFactory>().AsSingle();
    }
}
